MODEL_NAME = "RICH_ensemble"
PARTICLES = ['kaon', 'pion', 'proton', 'muon', 'electron']
PARTICLE_TYPE = 'pion'
CRAMER_DIM = 256
NUM_LAYERS = 5
BATCH_SIZE = int(1e2)
LATENT_DIMENSIONS = 64

N_VAL = 100
y_count = len(PARTICLES)
dll_columns = ['RichDLLe', 'RichDLLk', 'RichDLLmu', 'RichDLLp', 'RichDLLbt']
raw_feature_columns = ['Brunel_P', 'Brunel_ETA', 'nTracks_Brunel']
weight_col = 'probe_sWeight'
TEST_SIZE = 0.5