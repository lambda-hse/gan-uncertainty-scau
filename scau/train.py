import torch
import numpy as np
import os
import matplotlib.pyplot as plt
from itertools import repeat
from IPython.display import clear_output
from tqdm import tqdm_notebook, tqdm
from torch.autograd import Variable, grad

device = 'cuda' if torch.cuda.is_available() else 'cpu'


class ParticleSet(torch.utils.data.Dataset):
    def __init__(self, data):
        self.data = np.array(data)

    def __len__(self):
        return self.data.shape[0]

    def __getitem__(self, ind):
        return (
            self.data[ind, y_count:-1],  # X
            self.data[ind, -1],  # Weight
            self.data[ind, :y_count],  # DLL
        )


def repeater(data_loader):
    for loader in repeat(data_loader):
        for data in loader:
            yield data


def calc_gradient_penalty(netC, real_data, fake_data, fake_data2, batch_size=32):
    alpha = torch.rand(batch_size, 1)
    alpha = alpha.expand(real_data.size())
    alpha = alpha.to(device)

    interpolates = alpha * real_data + ((1 - alpha) * fake_data)
    disc_interpolates = cramer_critic(netC, interpolates, fake_data2)

    gradients = grad(outputs=disc_interpolates, inputs=interpolates,
                     grad_outputs=torch.ones(disc_interpolates.size()).to(device),
                     create_graph=True, retain_graph=True, only_inputs=True)[0]
    slopes = gradients.norm(2, dim=1)
    gradient_penalty = ((torch.max(torch.abs(slopes) - 1, torch.zeros_like(slopes))) ** 2).mean()
    return gradient_penalty


def cramer_critic(netC, x, y):
    discriminated_x = netC(x)
    return torch.norm(discriminated_x - netC(y), dim=1) - torch.norm(discriminated_x, dim=1)


def ensemble_penalty(netC, x, y):
    return torch.norm(netC(x) - netC(y), dim=1)


def get_set_predictions(x, netG):
    netG.eval()
    with torch.no_grad():
        generated = torch.cat([netG(x), x], dim=1)
    netG.train()

    return generated


def adversarial_loss(netC, netG, data):
    x, weight, dlls = data

    real_full_0 = torch.cat([dlls, x], dim=1)
    generated_1 = torch.cat([netG(x), x], dim=1)
    generated_2 = torch.cat([netG(x), x], dim=1)

    generator_loss = (cramer_critic(netC, real_full_0, generated_2) -
                      cramer_critic(netC, generated_1, generated_2)) * weight

    return real_full_0, generated_1, generated_2, generator_loss


def calculate_uncertainty_loss(netU, netC, netG, data):
    x, weight, dlls = data
    
    real_full_0 = torch.cat([dlls, x], dim=1)
    generated_1 = torch.cat([netG(x), x], dim=1)
    generated_2 = torch.cat([netG(x), x], dim=1)
    
    generator_loss = (cramer_critic(netC, real_full_0, generated_2) - 
                      cramer_critic(netC, generated_1, generated_2)) * weight
    
    uncertainty = netU(x)
    return torch.square(uncertainty - generator_loss)


def train_uncertainty_estimator_step(netU, netC, netG, netS, optU, data):
    optU.zero_grad()
    netC.eval()
    netG.eval()
    netU.train()
    
    x, weight, dlls = data
    
    weights = netS(x)
    _, uncertainty_weights = weights[:, 0], weights[:, 1]

    uncertainty = calculate_uncertainty_loss(netU, netC, netG, data) 
    uncertainty = torch.mean(uncertainty * uncertainty_weights)
    
    uncertainty.backward()
    optU.step()
    netU.eval()

    return uncertainty.item()


def train_separator_step(netS, netU, netC, netG, optS, data, reg_alpha):
    optS.zero_grad()
    netC.eval()
    netG.eval()
    netU.eval()
    netS.train()

    x, weight, dlls = data
    weights = netS(x)
    training_weights, uncertainty_weights = weights[:, 0], weights[:, 1]
    
    ### Regularisation part
    min_weights, _ = torch.min(weights, dim=1)
    alpha = reg_alpha * (16 * torch.square(min_weights) - 8 * min_weights + 1)
    ###
    
    _, _, _, adversarial = adversarial_loss(netC, netG, data)
    uncertainty = calculate_uncertainty_loss(netU, netC, netG, data) 
    total_loss = torch.mean(training_weights * uncertainty + uncertainty_weights * adversarial) + alpha
    
    if len(total_loss) > 0:
        total_loss = torch.mean(total_loss)

    total_loss.backward()
    optS.step()
    netS.eval()

    return total_loss.item()


def train_generator_step(netC, netG, optG, netS, data):
    optG.zero_grad()
    netC.eval()
    netS.eval()
    netG.train()
    
    x, weight, dlls = data

    weights = netS(x)
    training_weights, _ = weights[:, 0], weights[:, 1]
    _, _, _, generator_loss = adversarial_loss(netC, netG, data)
    assert(len(generator_loss) == len(training_weights))
    generator_loss = torch.mean(generator_loss * training_weights)

    generator_loss.backward()
    optG.step()

    return generator_loss.item()


def train_critic_step(netC, netG, optC, data, lambda_pt, iteration, batch_size):
    optC.zero_grad()
    netG.eval()
    netC.train()

    real_full_0, generated_1, generated_2, generator_loss = adversarial_loss(netC, netG, data)

    # GP
    gradient_penalty = calc_gradient_penalty(netC, real_full_0, generated_1, generated_2, batch_size)

    critic_loss = lambda_pt(iteration) * gradient_penalty - torch.mean(generator_loss)
    critic_loss.backward()
    optC.step()

    return critic_loss.item()

def train(train_data, val_data, path, netG, netC, netU, netS, device,
          batch_size=32, total_iter=int(1e5), reg_alpha=0.1, optG=None, optC=None, optU=None, optS=None):
    """
    Performs the models training.

    :param train_data: Training data.
    :param val_data: Validation data.
    :param path: Path where the models will be saved.
    :param netG: Generator.
    :param netC: Discriminator.
    :param netU: Uncertainty estimator.
    :param netS: Separator.
    :param device: The computing resource type.
    :param batch_size: The batch size.
    :param total_iter: Total number of the training steps.
    :param reg_alpha: Alpha used for the regularisation calculation.
    :param optG: Optimiser of the Generator.
    :param optC: Optimiser of the Discriminator.
    :param optU: Optimiser of the Uncertainty estimator.
    :param optS: Optimiser of the Separator.
    """
    train_loader = repeater(torch.utils.data.DataLoader(ParticleSet(train_data),
                                                        batch_size=batch_size,
                                                        shuffle=False,
                                                        pin_memory=True))
    val_loader = repeater(torch.utils.data.DataLoader(ParticleSet(val_data),
                                                      batch_size=N_VAL,
                                                      shuffle=False,
                                                      pin_memory=True))
    if optC is None:
        optC = torch.optim.RMSprop(netC.parameters(), lr=2e-4)
    if optG is None:
        optG = torch.optim.RMSprop(netG.parameters(), lr=2e-4)
    if optU is None:
        optU = torch.optim.RMSprop(netU.parameters(), lr=2e-4)
    if optS is None:
        optS = torch.optim.RMSprop(netS.parameters(), lr=2e-4)

    CRITIC_ITERATIONS_CONST = 15
    SEPARATOR_ITERATIONS_CONST = 5
    UNCERTAINTY_CONST = 15
    VALIDATION_INTERVAL = 100
    # lambda_pt = lambda i: 20 / np.pi * 2 * torch.atan(torch.tensor(i, dtype=torch.float32, device=device) / 1e4)
    lambda_pt = lambda i: 15.

    pt = []
    for i in tqdm_notebook(range(total_iter)):
        pt.append(lambda_pt(i))

    for iteration in tqdm(range(total_iter)):
        netG.train()
        netC.train()
        netU.train()
        netS.train()
        for critic_iter in range(CRITIC_ITERATIONS_CONST):
            x, weight, dlls = [i.to(device) for i in next(train_loader)]

            critic_loss = train_critic_step(netC, netG, optC, (x, weight, dlls),
                                            lambda_pt, iteration, batch_size=batch_size)
        for separator_iter in range(SEPARATOR_ITERATIONS_CONST):
            x, weight, dlls = [i.to(device) for i in next(train_loader)]
            separator_loss = train_separator_step(netS, netU, netC, netG, optS, (x, weight, dlls), reg_alpha)
        
        for uncertainty_iter in range(UNCERTAINTY_CONST):
            x, weight, dlls = [i.to(device) for i in next(train_loader)]
            uncertainty_loss = train_uncertainty_estimator_step(netU, netC, netG, netS, optU, (x, weight, dlls))

        x, weight, dlls = [i.to(device) for i in next(train_loader)]

        generator_loss = train_generator_step(netC, netG, optG, netS, (x, weight, dlls))

        netG.eval()
        netC.eval()
        netU.eval()
        netS.eval()
        
        ### Evaluation step 
        
        if iteration % VALIDATION_INTERVAL == 0:
            clear_output(False)
            print(f'{iteration} - '
                  f'generator_loss: {generator_loss} - '
                  f'critic_loss: {critic_loss} - '
                  f'separator_loss: {separator_loss} - '
                  f'uncertainty_loss: {uncertainty_loss}')
            print('skip plot')

            with torch.no_grad():
                x, weight, dlls = [i.to(device) for i in next(val_loader)]
                preds = netG(x)
                fig, axes = plt.subplots(2, 5, figsize=(45, 15))
                for INDEX, ax in zip((0, 1, 2, 3, 4), axes.flatten()[:5]):
                    _, bins, _ = ax.hist(dlls[:, INDEX].cpu(),
                                         bins=100,
                                         label="data")
                    for i, p in enumerate([preds]):
                        ax.hist(p[:, INDEX].cpu(),
                                bins=bins,
                                label=f"generated {i}",
                                alpha=0.5)
                    ax.legend()
                    ax.set_title(dll_columns[INDEX])

                plt.show()

        if iteration % VALIDATION_INTERVAL == 0:
            separator_pt = 'separator_weights.pt'
            uncertainty_pt = 'uncertainty_estimator_weights.pt'
            generators_pt = 'generators_without_weights.pt'
            critic_pt = 'critic_without_weights.pt'
            opt_dict = {f'{i}_optimizer_state_dict': optG.state_dict()}
            torch.save({**{
                'iteration': iteration,
                'model_state_dict': netG.state_dict(),
                'loss': generator_loss
            }, **opt_dict}, os.path.join(path, generators_pt))

            opt_dict = {f'{i}_optimizer_state_dict': optC.state_dict()}
            torch.save({**{
                'iteration': iteration,
                'model_state_dict': netC.state_dict(),
                'loss': critic_loss
            }, **opt_dict}, os.path.join(path, critic_pt))

            opt_dict = {f'{i}_optimizer_state_dict': optU.state_dict()}
            torch.save({**{
                'iteration': iteration,
                'model_state_dict': netU.state_dict(),
                'loss': uncertainty_loss
            }, **opt_dict}, os.path.join(path, uncertainty_pt))

            opt_dict = {f'{i}_optimizer_state_dict': optS.state_dict()}
            torch.save({**{
                'iteration': iteration,
                'model_state_dict': netS.state_dict(),
                'loss': separator_loss
            }, **opt_dict}, os.path.join(path, separator_pt))

            print('model saved')


