import torch
from torch import nn
from .params import *


device = 'cuda' if torch.cuda.is_available() else 'cpu'


def get_noise(batch_size, mean=0, std=1):
    noise = torch.zeros(batch_size, LATENT_DIMENSIONS)
    normalised = noise.normal_(mean, std)
    return normalised.to(device)


def init_weights(m):
    if type(m) == nn.Linear:
        torch.nn.init.xavier_uniform_(m.weight)
        m.bias.data.fill_(0.01)


class Separator(nn.Module):
    """
    For one object X returns two weights `W1` and `W2` where `W2` = 1 - `W1`.

    The given object X is used with weight `W1` during the calculation of the
    training loss and with weight `W2` during the calculation of the
    calibration loss.
    """
    def __init__(self, hidden_size=128, depth=3, s=9):
        super(Separator, self).__init__()
        self.layers = nn.Sequential(
            nn.Linear(LATENT_DIMENSIONS + s - 1 - y_count, hidden_size),
            nn.ReLU(),
            *[nn.Sequential(nn.Linear(hidden_size, hidden_size), nn.LeakyReLU(0.05)) for _ in range(depth)],
            nn.Linear(hidden_size, 2),
            nn.Softmax(dim=1),
        )
        self.apply(init_weights)

    def forward(self, x, noise=None):
        return self.layers(torch.cat([x, get_noise(x.shape[0])], dim=1))


class UncertaintyEstimator(nn.Module):
    """
    For the object X returns an uncertainty estimate `U`.
    """

    def __init__(self, hidden_size=128, depth=3, s=9):
        super(UncertaintyEstimator, self).__init__()
        self.layers = nn.Sequential(
            nn.Linear(LATENT_DIMENSIONS + s - 1 - y_count, hidden_size),
            nn.ReLU(),
            *[nn.Sequential(nn.Linear(hidden_size, hidden_size), nn.LeakyReLU(0.05)) for _ in range(depth)],
            nn.Linear(hidden_size, 1),
        )
        self.apply(init_weights)

    def forward(self, x, noise=None):
        return self.layers(torch.cat([x, get_noise(x.shape[0])], dim=1))


class Generator(nn.Module):
    def __init__(self, hidden_size=128, depth=5, s=9):
        super(Generator, self).__init__()
        self.layers = nn.Sequential(
            nn.Linear(LATENT_DIMENSIONS + s - 1 - y_count, hidden_size),
            nn.ReLU(),
            *[nn.Sequential(nn.Linear(hidden_size, hidden_size), nn.LeakyReLU(0.05)) for _ in range(depth)],
            nn.Linear(hidden_size, y_count),
        )
        self.apply(init_weights)

    def forward(self, x, noise=None):
        return self.layers(torch.cat([x, get_noise(x.shape[0])], dim=1))

    
class GeneratorWithDropout(nn.Module):
    def __init__(self, hidden_size=128, depth=5, s=9):
        super(Generator, self).__init__()
        self.layers = nn.Sequential(
            nn.Linear(LATENT_DIMENSIONS + s - 1 - y_count, hidden_size),
            nn.ReLU(),
            *[nn.Sequential(nn.Linear(hidden_size, hidden_size), nn.LeakyReLU(0.05), nn.Dropout(0.2)) for _ in range(depth)],
            nn.Linear(hidden_size, y_count),
        )
        self.apply(init_weights)

    def forward(self, x, noise=None):
        return self.layers(torch.cat([x, get_noise(x.shape[0])], dim=1))


class Critic(nn.Module):
    def __init__(self, hidden_size=128, depth=5, s=9):
        super(Critic, self).__init__()
        self.layers = nn.Sequential(
            nn.Linear(s - 1, hidden_size),
            nn.ReLU(),
            *[nn.Sequential(nn.Linear(hidden_size, hidden_size), nn.LeakyReLU(0.05)) for _ in range(depth)],
            nn.Linear(hidden_size, CRAMER_DIM),
        )
        self.apply(init_weights)

    def forward(self, x):
        return self.layers(x)
