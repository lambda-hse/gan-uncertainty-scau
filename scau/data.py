import pandas as pd
from .params import *
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import RobustScaler, QuantileTransformer, StandardScaler
from time import time


def load_and_merge_and_cut(filename_list):
    return pd.concat([load_and_cut(fname) for fname in filename_list], axis=0, ignore_index=True)


def load_and_cut(file_name):
    data = pd.read_csv(file_name, delimiter='\t')
    return data[dll_columns+raw_feature_columns+[weight_col]]


def split(data):
    data_train, data_val = train_test_split(data, test_size=TEST_SIZE, random_state=42)
    data_val, data_test = train_test_split(data_val, test_size=TEST_SIZE, random_state=1812)
    return data_train.reset_index(drop=True), \
           data_val  .reset_index(drop=True), \
           data_test .reset_index(drop=True)


def scale_pandas(dataframe, scaler):
    return pd.DataFrame(scaler.transform(dataframe.values), columns=dataframe.columns)


def get_val_dataset(datasets, particle_type, dtype=None, log=False, n_quantiles=100000):
    file_list = datasets[particle_type]
    if log:
        print("Reading and concatenating datasets:")
        for fname in file_list: print("\t{}".format(fname))
    data_full = load_and_merge_and_cut(file_list)
    # Must split the whole to preserve train/test split""
    if log:
        print("splitting to train/val/test")
    data_train, data_val, data_test = split(data_full)
    if log: print("fitting the scaler")
    print("scaler train sample size: {}".format(len(data_train)))
    start_time = time()
    if n_quantiles == 0:
        scaler = StandardScaler().fit(data_train.drop(weight_col, axis=1).values)
    else:
        scaler = QuantileTransformer(output_distribution="normal",
                                     n_quantiles=n_quantiles,
                                     subsample=int(1e10)).fit(data_train.drop(weight_col, axis=1).values)
    print("scaler n_quantiles: {}, time = {}".format(n_quantiles, time() - start_time))
    if log: print("scaling train set")
    data_train_s = pd.concat([scale_pandas(data_train.drop(weight_col, axis=1), scaler), data_train[weight_col]],
                             axis=1)
    if log: print("scaling val set")
    data_val_s = pd.concat([scale_pandas(data_val.drop(weight_col, axis=1), scaler), data_val[weight_col]], axis=1)
    if log: print("scaling val set")
    data_test_s = pd.concat([scale_pandas(data_test.drop(weight_col, axis=1), scaler), data_test[weight_col]], axis=1)

    if dtype is not None:
        if log: print("converting dtype to {}".format(dtype))
        data_train = data_train.astype(dtype, copy=False)
        data_val = data_val.astype(dtype, copy=False)
        data_test = data_test.astype(dtype, copy=False)

        data_train_s = data_train_s.astype(dtype, copy=False)
        data_val_s = data_val_s.astype(dtype, copy=False)
        data_test_s = data_test_s.astype(dtype, copy=False)

    return data_train, data_val, data_test, data_train_s, data_val_s, data_test_s, scaler