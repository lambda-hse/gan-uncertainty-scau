# GAN uncertainty via self-calibrating ensembles

The repository contains an implementation of the SCAU method and all the experiments with it, including the ones which were made by splitting the RICH dataset by rings and by lines. Implementation of the models can be found in `scau/nets.py`, their training -- in `scau/train.py`. The main Jupyter Notebook `train_and_eval.ipynb` contains all the training and evaluation steps.
